DokuWiki docker image
=====================

Locations
---------

[Source code repository][1] is hosted on Bitbucket.

Please report any problem in the [issue tracker][2].

The image can be pulled from [Docker Hub repository][3].

[1]: https://bitbucket.org/beli-sk/docker-dokuwiki "DokuWiki image source"
[2]: https://bitbucket.org/beli-sk/docker-dokuwiki/issues "Issue tracker"
[3]: https://hub.docker.com/r/beli/dokuwiki
