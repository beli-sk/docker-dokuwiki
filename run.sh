#!/bin/bash

cat > /etc/dokuwiki/local.php << _EOF
<?php
\$conf['title'] = '${DW_TITLE:=DokuWiki}';
\$conf['license'] = '${DW_LICENSE:=cc-by-sa}';
${DW_LANG:+#}\$conf['lang'] = '${DW_LANG}';
\$conf['useacl'] = ${DW_USEACL:=1};
\$conf['superuser'] = '${DW_SUPERUSER:=@admin}';
\$conf['userewrite'] = 1;
?>
_EOF

if [[ -z "`ls /data/`" ]] ; then
	echo "Volume /data empty, initializing..."
	cp -a /data.dist/. /data/
fi

if [[ -z "`ls /acl/`" ]] ; then
	echo "Volume /acl empty, initializing with user 'admin' password 'admin'..."
	echo "PLEASE CHANGE THE DEFAULT PASSWORD."
	cp -a /acl.dist/. /acl/
fi

. /etc/apache2/envvars

exec /usr/sbin/apache2 -DFOREGROUND
