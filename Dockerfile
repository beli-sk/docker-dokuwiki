FROM beli/ubuntu-minimal:14.04
MAINTAINER Michal Belica <devel@beli.sk>

RUN sed -ie 's/^\(deb.* main\)[[:space:]]*$/\1 universe/' /etc/apt/sources.list
RUN apt-get update && apt-get -y --no-install-recommends install apache2 libapache2-mod-php5 dokuwiki && apt-get clean
RUN find /var/lib/apt/lists/ -type f -delete

COPY apache.conf /etc/dokuwiki/
RUN chown root:root /etc/dokuwiki/apache.conf

RUN a2enmod rewrite

COPY run.sh /
RUN chown root:root /run.sh ; chmod 0755 /run.sh

RUN mv /var/lib/dokuwiki/data / && cp -a /data /data.dist && ln -s /data /var/lib/dokuwiki/
# admin:admin
RUN mv /var/lib/dokuwiki/acl / \
	&& echo "admin:21232f297a57a5a743894a0e4a801fc3:DokuWiki Administrator:webmaster@localhost:admin,user" \
	> /acl/users.auth.php \
	&& cp -a /acl /acl.dist \
	&& ln -s /acl /var/lib/dokuwiki/

EXPOSE 80
VOLUME /data
VOLUME /acl

CMD ["/run.sh"]
